// components/navBar/navBar.js
var app = getApp()
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        // 标题
        title:{
            type:String,
            default:""
        },
        // 回到首页按钮
        home:{
            type:Boolean,
            default:false
        },
        // 返回按钮
        back:{
            type:Boolean,
            default:false
        },
        // 回到首页的页面
        homePage:{
            type:String,
            default:""
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        statusBarHeight: '',
        MenuButton: '',
        MenuObject: {},
        customStyle: '',
        statusBar: {},
        customLeftStyle: ''
    },

    /**
     * 组件的方法列表
     */
    lifetimes: {
        attached() {
            if(getCurrentPages().length < 2){
                this.setData({
                    back:false
                })
            }
            this.computedNavBar();
        },
    },

    methods: {
        backClick(){
            wx.navigateBack({
              delta: 1,
            })
        },
        homeClick(){
            let pages = this.properties.homePage;
            console.log(pages)
            wx.reLaunch({
              url: pages,
            })
        },
        computedNavBar() {
            //自定义navbar
            wx.getSystemInfo({
                success: res => {
                    // 获取系统信息
                    let statusBar = res;
                    // 获取状态栏高度
                    let statusBarHeight = res.statusBarHeight;
                    // 获取状态栏高度
                    let MenuButton = wx.getMenuButtonBoundingClientRect().height + (wx.getMenuButtonBoundingClientRect().top - statusBarHeight) + 8;
                    // 获取胶囊信息
                    let MenuObject = wx.getMenuButtonBoundingClientRect();
                    // 获取状态栏高度
                    let height = 'height:' + Number(statusBarHeight + MenuButton) + 'px'
                    let paddingBottom = 'padding-bottom:' + (Number(statusBarHeight + MenuButton) - Number(MenuObject.height + MenuObject.top)) + 'px'
                    let paddingRight = 'padding-right:' + Number((statusBar.screenWidth - MenuObject.right) + MenuObject.width) + 'px';
                    let paddingTop = 'padding-top:' + Number(MenuObject.top) + 'px';
                    let LeftWidth = 'width:' + Number(MenuObject.width) + 'px';
                    let LeftHeight = 'height:' + Number(MenuObject.height) + 'px';
                    let LeftPaddingLeft = 'padding-left:' + Number(statusBar.screenWidth - MenuObject.right) + 'px';
                    let RightPaddingRight = 'padding-right:' + Number(statusBar.screenWidth - MenuObject.right) + 'px';
                    let scrollTop = 0;
                    setInterval(() => {
                        wx.createSelectorQuery().in(this).selectViewport("#navBar").scrollOffset(function(rect){
                            console.log(rect)
                        }).exec()
                    }, 100);
                    
                    this.setData({
                        customStyle: height + ';' + paddingBottom + ';' + paddingRight + ';' + paddingTop,
                        customLeftStyle: LeftHeight + ';' + LeftPaddingLeft + ';' + LeftWidth + ';',
                        LeftPaddingLeft: LeftPaddingLeft + ';' +RightPaddingRight,
                    })
                }
            })
        }
    },

})